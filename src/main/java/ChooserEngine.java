import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;


public class ChooserEngine implements ActionListener {
	private final MainForm parent;

	public ChooserEngine(MainForm parent) {
		this.parent = parent;
	}

	public void actionPerformed(ActionEvent e) {
		JButton src = (JButton) e.getSource();
		JFileChooser chooser = new JFileChooser();
		int ret = chooser.showDialog(parent, "Выберете файл");

		if (ret == JFileChooser.APPROVE_OPTION) {
			File selectedFile;
			selectedFile = chooser.getSelectedFile();
			if (src == parent.hostButton) {
				readFile(selectedFile);
			}
			if (src == parent.fileButton) {
				parent.sendingFile = selectedFile;
				parent.fileName.setText(selectedFile.getPath());
			}
		}
	}

	private void readFile(File selectedFile) {

		try {
			FileInputStream fstream = new FileInputStream(selectedFile);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				parent.spisHost.append(strLine + "\n");
				parent.hostPass.add(strLine);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}


	}
}
