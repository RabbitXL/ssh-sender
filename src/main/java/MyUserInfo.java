import com.jcraft.jsch.UIKeyboardInteractive;
import com.jcraft.jsch.UserInfo;

import javax.swing.*;


public class MyUserInfo implements UserInfo, UIKeyboardInteractive {


	public MyUserInfo(String passwd) {
		this.passwd = passwd;
	}

	public String getPassword() {
		return passwd;
	}

	public boolean promptYesNo(String str) {
		return true;
	}

	private String passwd;


	public String getPassphrase() {
		return null;
	}

	public boolean promptPassphrase(String message) {
		return true;
	}

	public boolean promptPassword(String message) {

		return true;

	}

	public void showMessage(String message) {
		JOptionPane.showMessageDialog(null, message);
	}


	public String[] promptKeyboardInteractive(String destination,
											  String name,
											  String instruction,
											  String[] prompt,
											  boolean[] echo) {


		String[] texts = new String[prompt.length];
		for (int i = 0; i < prompt.length; i++) {

			if (echo[i]) {
				texts[i] = new String();
			} else {
				texts[i] = this.passwd;
			}
		}

		String[] response = new String[prompt.length];
		for (int i = 0; i < prompt.length; i++) {
			response[i] = texts[i];
		}
		return response;


	}
}
