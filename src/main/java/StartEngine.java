import com.jcraft.jsch.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class StartEngine implements ActionListener {

	private final MainForm parent;


	public StartEngine(MainForm parent) {
		this.parent = parent;
	}

	public void actionPerformed(ActionEvent e) {
		//todo продумать многопоточность
		ExecutorService pool = Executors.newCachedThreadPool();
		//for (int i = 0;i<=parent.spisHost.getLineCount();i++){
		//     parent.hostPass.add(parent.spisHost.getText());
		// }
		for (final String currentHost : parent.hostPass) {
			pool.submit(new Runnable() {
				@Override
				public void run() {
					potok(currentHost);
				}
			});
		}
	}

	private void potok(String currentHost) {
		JSch jsch;
		jsch = new JSch();
		int port = 3000;
		UserInfo ui;
		Session session = null;
		String user = currentHost.substring(0, currentHost.indexOf("@"));
		String host = currentHost.substring(currentHost.indexOf("@") + 1, currentHost.indexOf(":"));
		String pass = currentHost.substring(currentHost.indexOf(":") + 1);

		try {
			session = jsch.getSession(user, host, port);
		} catch (JSchException e1) {
			e1.printStackTrace();
		}

		ui = new MyUserInfo(pass);
		session.setUserInfo(ui);
		try {
			session.connect();
		} catch (JSchException e1) {
			e1.printStackTrace();
		}
		Channel channel = null;
		try {
			channel = session.openChannel("sftp");
		} catch (JSchException e1) {
			e1.printStackTrace();
		}
		try {
			channel.connect();
		} catch (JSchException e1) {
			e1.printStackTrace();
		}
		final ChannelSftp c = (ChannelSftp) channel;

		final SftpProgressMonitor monitor = new NewProgressMonitor(currentHost);
		final int mode = ChannelSftp.OVERWRITE;


		try {
			c.put(parent.sendingFile.getAbsolutePath(), parent.remotePath.getText(), monitor, mode);
		} catch (SftpException e1) {
			e1.printStackTrace();
		}

	}
}
