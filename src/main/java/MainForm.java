import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;

public class MainForm extends JFrame {
	JButton hostButton; //кнопка выбора списка хостов
	JButton fileButton; //кнопка выбора файла для отправки
	JTextField fileName; // поле с полным именем фала
	JTextField remotePath; // путь для сохраниния на удаленной машине
	JTextArea spisHost; // поле со списком хостов
	JButton startButton; // кнопка старт
	ArrayList<String> hostPass = new ArrayList<String>(); // массив хостов, логинов и паролей
	File sendingFile; // отправляемый хост

	// конструктор главной формы
	//todo подумать над UI
	public MainForm() {
		super("SSH Java Sender");
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); // закрываем форму по нажатию крестика
		setContentPane(createPanel()); //создаем и заполняем центральную панель
		pack();
	}

	//создание панели
	private Container createPanel() {
		JPanel centerPanel = new JPanel();//главная панель
		centerPanel.setLayout(new MigLayout());

		spisHost = new JTextArea(10, 40);
		//spisHost.setPreferredSize(new Dimension(50,20));
		spisHost.setToolTipText("login@host:password");
		JLabel spisLabel = new JLabel("Список хостов:");

		hostButton = new JButton("Список хостов из файла");
		ChooserEngine chEn = new ChooserEngine(this);
		hostButton.addActionListener(chEn);
		centerPanel.add(spisLabel);
		centerPanel.add(spisHost);
		centerPanel.add(hostButton, "wrap");

		JLabel fileLabel = new JLabel("Файл для отправки");

		fileName = new JTextField(40);
		//fileName.setPreferredSize(new Dimension(400,20));

		fileButton = new JButton("Выбрать...");
		fileButton.addActionListener(chEn);
		centerPanel.add(fileLabel);
		centerPanel.add(fileName);
		centerPanel.add(fileButton, "wrap");
		JLabel remoteLabel = new JLabel("Путь для сохранения");
		remotePath = new JTextField(40);
		startButton = new JButton("Пуск");
		StartEngine acEn = new StartEngine(this);
		startButton.addActionListener(acEn);
		centerPanel.add(remoteLabel);
		centerPanel.add(remotePath);
		centerPanel.add(startButton);

		return centerPanel;
	}
}
