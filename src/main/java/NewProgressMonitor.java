import com.jcraft.jsch.SftpProgressMonitor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//todo сделать так чтобы окна прогресса не перекрывали друг друга
public class NewProgressMonitor extends JFrame implements SftpProgressMonitor, Runnable {

	long max = 0;
	long count = 0;
	private long percent = -1;
	String host;
	String operation;
	JProgressBar bar;
	JPanel windowContent;
	JButton cancel;
	JLabel upLabel;
	JLabel centerLabel;
	JLabel downLabel;
	boolean isCanceled = false;

	public NewProgressMonitor(String host) {
		super(host);
		this.host = host;

		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		windowContent = new JPanel();
		windowContent.setLayout(new FlowLayout());

		bar = new JProgressBar();
		bar.setIndeterminate(true);
		bar.setPreferredSize(new Dimension(400, 10));
		bar.setMaximum((int) max);
		cancel = new JButton("Отмена");
		cancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				isCanceled = true;
			}
		});
		upLabel = new JLabel("");
		centerLabel = new JLabel("");
		downLabel = new JLabel("");
		windowContent.add(upLabel, "wrap");
		windowContent.add(downLabel, "wrap");
		windowContent.add(bar, "wrap");
		windowContent.add(cancel);
		setContentPane(windowContent);
		setPreferredSize(new Dimension(500, 150));
		pack();
		setVisible(true);
	}

	@Override
	public void init(int op, String src, String dest, long max) {
		this.max = max;
		bar.setMinimum(0);
		bar.setMaximum((int) max);

		if (op == SftpProgressMonitor.PUT) {
			operation = "Отправка ";
		} else {
			operation = "Получение ";
		}
		upLabel.setText(operation + src);
		centerLabel.setText(dest);
		downLabel.setText("");
		count = 0;
		percent = -1;
		run();
	}

	@Override
	public boolean count(final long count) {
		this.count += count;

		if (percent >= this.count * 100 / max) {
			return true;
		}
		;
		percent = this.count * 100 / max;
		downLabel.setText("Скопировано " + this.count + "(" + percent + "%) out of " + max + ".");


		return !(isCanceled);
	}

	@Override
	public void end() {
		setVisible(false);
		dispose();
	}

	@Override
	public void run() {
		bar.setValue((int) count);
	}
}
